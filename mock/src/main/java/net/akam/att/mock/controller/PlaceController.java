package net.akam.att.mock.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.akam.att.mock.data.Place;

@RestController
@RequestMapping("/api/places/")
public class PlaceController extends BaseController {

    @GetMapping(value = "/company{id}")
    public ResponseEntity<?> getByCompanyId(@PathVariable String id) {
        return success(Place.COM1);
    }

    /*
     * @PutMapping(value = "/{id}") public ResponseEntity<?> update(@ String id) {
     * return success(Place.COM1); }
     */

}
