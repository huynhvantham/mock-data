package net.akam.att.mock.controller;

import java.net.URI;
import java.time.Instant;
import java.time.ZonedDateTime;

import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import net.akam.att.mock.model.Json;
import net.akam.att.mock.model.Response;

public class BaseController extends ResponseEntityExceptionHandler{

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public <T> ResponseEntity<?> get(int status, String message, @Nullable T body, HttpStatus httpStatus) {
        return new ResponseEntity(new Response<T>(status, message, body), httpStatus);
    }

    public <T> ResponseEntity<?> unknown(T body) {
        return get(0, "Some thing went wrong :(", null, HttpStatus.OK);
    }

    public <T> ResponseEntity<?> success(String message, T body) {
        return get(200, message, body, HttpStatus.OK);
    }

    public <T> ResponseEntity<?> success(String message, Json json) {
        return success(message, json == null ? null : json.build());
    }

    public <T> ResponseEntity<?> success(T body) {
        return success("success", body);
    }

    public <T> ResponseEntity<?> success(Json json) {
        return success("success", json == null ? null : json.build());
    }

    public <T> ResponseEntity<?> success() {
        return success(null);
    }

    public <T> ResponseEntity<?> failed(String message) {
        return new ResponseEntity<>(new Response<T>(204, message, null), HttpStatus.OK);
    }

    public ResponseEntity<?> failed() {
        return failed("failed");
    }
    
    
    public <T> ResponseEntity<?> badRequest(String s, String description) {
        String message = String.format("required parameter missing or invalid -> %s (%s)", s, description);
        return new ResponseEntity<>(new Response<T>(400, message, null), HttpStatus.BAD_REQUEST);
    }

    public <T> ResponseEntity<?> badRequest(String s) {
        String message = String.format("required parameter missing or invalid: %s", s);
        return new ResponseEntity<>(new Response<T>(400, message, null), HttpStatus.BAD_REQUEST);
    }

}
