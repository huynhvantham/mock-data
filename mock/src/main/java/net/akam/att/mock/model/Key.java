package net.akam.att.mock.model;

public class Key {

    public static final String ID = "id";
    public static final String EMAIL_PHONE = "phoneOrEmail";
    public static final String EMAIL = "email";
    public static final String PHONE = "phone";
    public static final String PASSWORD = "password";
    public static final String TOKEN = "token";
    public static final String TOKEN_TYPE = "tokenType";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String BIRTH = "birth";
    public static final String MALE = "male";
    public static final String AVATAR = "avatar";
    public static final String PROFRESSIONAL = "professional";
    public static final String COUNTRY = "country";
    public static final String LOCATION = "location";
    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String CONTACT_EMAIL = "contact_email";
    public static final String CONTACT_PHONE = "contact_phone";
    public static final String ADDRESS = "address";
    public static final String KEYWORD = "keyword";
    public static final String START_INDEX = "startIndex";
    public static final String END_INDEX = "endIndex";
    public static final String COM_ID = "comId";

}
