package net.akam.att.mock.model;

import java.util.HashMap;

public class Body extends HashMap<String, Object> {

    private static final long serialVersionUID = 1L;

    public String string(String key) {
        return get(key) == null ? null : get(key).toString();
    }

    public Integer integer(String key) {
        return get(key) == null ? null : Integer.valueOf(get(key).toString());
    }

    public Boolean bool(String key) {
        return Boolean.valueOf(get(Key.MALE).toString());
    }

}
