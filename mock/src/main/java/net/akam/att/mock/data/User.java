package net.akam.att.mock.data;

import java.util.List;
import java.util.Map;
import net.akam.att.mock.model.Json;
import net.akam.att.mock.model.Key;
import net.akam.att.mock.utils.MapUtils;

public class User {

    public static final Json CURRENT_USER = new Json()
            .put(Key.ID, 1).put(Key.EMAIL, "huyqv.softlink@gmail.com")
            .put(Key.PHONE, "84935491290").put(Key.FIRST_NAME, "Donald").put(Key.LAST_NAME, "Trump JR")
            .put(Key.BIRTH, "1946-06-14").put(Key.MALE, true).put(Key.PROFRESSIONAL, "President").put(Key.COUNTRY, "US")
            .put(Key.AVATAR, "1.jpg").put(Key.LOCATION, "Washington DC").put(Key.TOKEN_TYPE, "Bearer")
            .put(Key.TOKEN, "qwertyuiopasdfghjkl.zxcvbnm12345678");

    public static final Json CURRENT_USER_UPDATED = new Json()
            .put(Key.ID, 1).put(Key.EMAIL, "huyqv.softlink@gmail.com")
            .put(Key.PHONE, "84935491290").put(Key.FIRST_NAME, "Donald-Updated").put(Key.LAST_NAME, "Trump JR-Updated")
            .put(Key.BIRTH, "1946-06-14").put(Key.MALE, true).put(Key.PROFRESSIONAL, "President - Updated").put(Key.COUNTRY, "US")
            .put(Key.AVATAR, "1.jpg").put(Key.LOCATION, "Washington DC - Updated");
    
    public static final Map<String, Object> U1 = MapUtils.getUser(1, "huyqv.softlink@gmail.com", "84935491290",
            "Donald", "Trump JR", true, "US", "Washington DC", "President", "1946-06-14", "1.jpg", 1);
    public static final Map<String, Object> U2 = MapUtils.getUser(2, "example2@email.com", null, "Aaron", "Ramsey",
            true, "England", "London", "Midfielder", "1946-06-14", "2.jpg", 0);
    public static final Map<String, Object> U3 = MapUtils.getUser(3, "example3@email.com", null, "Alexandre",
            "Lacazette", true, "England", "London", "Forward", "1946-06-14", "3.jpg", 0);
    public static final Map<String, Object> U4 = MapUtils.getUser(4, "example4@email.com", null, "Alexis", "Sanchez",
            true, "England", "Manchester", "Midfielder", "1946-06-14", "4.jpg", 0);
    public static final Map<String, Object> U5 = MapUtils.getUser(5, "example5@email.com", null, "Ander", "Herrera",
            true, "England", "Manchester", "Midfielder", "1946-06-14", "5.jpg", 0);
    public static final Map<String, Object> U6 = MapUtils.getUser(6, "example6@email.com", null, "Anthony", "Martial",
            true, "England", "Manchester", "Forward", "1946-06-14", "6.jpg", 0);
    public static final Map<String, Object> U7 = MapUtils.getUser(7, "example7@email.com", null, "Antoine", "Griezmann",
            true, "Spain", "Madrid", "Forward", "1946-06-14", "7.jpg", 0);
    public static final Map<String, Object> U8 = MapUtils.getUser(8, "example8@email.com", null, "Cristiano", "Ronaldo",
            true, "Italy", "Turin", "Forward", "1946-06-14", "8.jpg", 0);
    public static final Map<String, Object> U9 = MapUtils.getUser(9, "example9@email.com", null, "David", "Luiz", true,
            "England", "London", "Defender", "1946-06-14", "9.jpg", 0);
    public static final Map<String, Object> U10 = MapUtils.getUser(10, "example10@email.com", null, "David", "De Gea",
            true, "England", "Manchester", "Goalkeeper", "1946-06-14", "10.jpg", 0);
    public static final Map<String, Object> U11 = MapUtils.getUser(11, "example11@email.com", null, "Eden", "Hazard",
            true, "England", "London", "Midfielder", "1946-06-14", "11.jpg", 0);
    public static final Map<String, Object> U12 = MapUtils.getUser(12, "example12@email.com", null, "Edinson", "Cavani",
            true, "France", "Paris", "Forward", "1946-06-14", "12.jpg", 0);
    public static final Map<String, Object> U13 = MapUtils.getUser(13, "example13@email.com", null, "Fernando",
            "Torres", true, "Spain", "Madrid", "Forward", "1946-06-14", "13.jpg", 0);
    public static final Map<String, Object> U14 = MapUtils.getUser(14, "example14@email.com", null, "Gareth", "Bale",
            true, "Spain", "Madrid", "Forward", "1946-06-14", "14.jpg", 0);
    public static final Map<String, Object> U15 = MapUtils.getUser(15, "example15@email.com", null, "Gianluigi",
            "Buffon", true, "Italy", "Turin", "Goalkeeper", "1946-06-14", "15.jpg", 0);
    public static final Map<String, Object> U16 = MapUtils.getUser(16, "example16@email.com", null, "Harry", "Kane",
            true, "England", "London", "Forward", "1946-06-14", "16.jpg", 0);
    public static final Map<String, Object> U17 = MapUtils.getUser(17, "example17@email.com", null, "Henrikh",
            "Mkhitaryan", true, "England", "London", "Forward", "1946-06-14", "17.jpg", 0);
    public static final Map<String, Object> U18 = MapUtils.getUser(18, "example18@email.com", null, "Ilkay", "Gundogan",
            true, "England", "Manchester", "Midfielder", "1946-06-14", "18.jpg", 0);
    public static final Map<String, Object> U19 = MapUtils.getUser(19, "example19@email.com", null, "Jesse", "Lingard",
            true, "England", "Manchester", "Forward", "1946-06-14", "19.jpg", 0);
    public static final Map<String, Object> U20 = MapUtils.getUser(20, "example20@email.com", null, "Juan", "Mata",
            true, "England", "Manchester", "Midfielder", "1946-06-14", "20.jpg", 0);
    public static final Map<String, Object> U21 = MapUtils.getUser(21, "example21@email.com", null, "Karim", "Benzama",
            true, "Spain", "Madrid", "Forward", "1946-06-14", "21.jpg", 0);
    public static final Map<String, Object> U22 = MapUtils.getUser(22, "example22@email.com", null, "Kevin",
            "de Bruyne", true, "England", "Manchester", "Midfielder", "1946-06-14", "22.jpg", 0);
    public static final Map<String, Object> U23 = MapUtils.getUser(23, "example23@email.com", null, "Kylian", "Mbappe",
            true, "France", "Paris", "Forward", "1946-06-14", "23.jpg", 0);
    public static final Map<String, Object> U24 = MapUtils.getUser(24, "example24@email.com", null, "Leroy", "Sane",
            true, "England", "Manchester", "Forward", "1946-06-14", "24.jpg", 0);
    public static final Map<String, Object> U25 = MapUtils.getUser(25, "example25@email.com", null, "Lione", "Messi",
            true, "Spain", "Catalonia", "Forward", "1946-06-14", "25.jpg", 0);
    public static final Map<String, Object> U26 = MapUtils.getUser(26, "example26@email.com", null, "Luis", "Suarez",
            true, "Spain", "Catalonia", "Forward", "1946-06-14", "26.jpg", 0);
    public static final Map<String, Object> U27 = MapUtils.getUser(27, "example27@email.com", null, "Luka", "Modric",
            true, "Spain", "Madrid", "Midfielder", "1946-06-14", "27.jpg", 0);
    public static final Map<String, Object> U28 = MapUtils.getUser(28, "example28@email.com", null, "Luke", "Shaw",
            true, "England", "Manchester", "Defender", "1946-06-14", "28.jpg", 0);
    public static final Map<String, Object> U29 = MapUtils.getUser(29, "example29@email.com", null, "Marcelo", "Vieira",
            true, "Spain", "Madrid", "Defender", "1946-06-14", "29.jpg", 0);
    public static final Map<String, Object> U30 = MapUtils.getUser(30, "example30@email.com", null, "Marco", "Reus",
            true, "Germany", "Dortmund", "Midfielder", "1946-06-14", "30.jpg", 0);
    public static final Map<String, Object> U31 = MapUtils.getUser(31, "example31@email.com", null, "Marcus",
            "Rashford", true, "England", "Manchester", "Forward", "1946-06-14", "31.jpg", 0);
    public static final Map<String, Object> U32 = MapUtils.getUser(32, "example32@email.com", null, "Mesut", "Ozil",
            true, "England", "London", "Midfielder", "1946-06-14", "32.jpg", 0);
    public static final Map<String, Object> U33 = MapUtils.getUser(33, "example33@email.com", null, "Mohamed", "Salah",
            true, "England", "Merseyside", "Forward", "1946-06-14", "33.jpg", 0);
    public static final Map<String, Object> U34 = MapUtils.getUser(34, "example34@email.com", null, "Nemanja", "Matic",
            true, "England", "Manchester", "Midfielder", "1946-06-14", "34.jpg", 0);
    public static final Map<String, Object> U35 = MapUtils.getUser(35, "example35@email.com", null, "Neymar",
            "da Silva Santos", true, "France", "Paris", "Forward", "1946-06-14", "35.jpg", 0);
    public static final Map<String, Object> U36 = MapUtils.getUser(36, "example36@email.com", null, "Olivier", "Giroud",
            true, "England", "London", "Forward", "1946-06-14", "36.jpg", 0);
    public static final Map<String, Object> U37 = MapUtils.getUser(37, "example37@email.com", null, "Paul", "Pogba",
            true, "England", "Manchester", "Midfielder", "1946-06-14", "37.jpg", 0);
    public static final Map<String, Object> U38 = MapUtils.getUser(38, "example38@email.com", null, "Paulo", "Dybala",
            true, "Italy", "Turin", "Forward", "1946-06-14", "38.jpg", 0);
    public static final Map<String, Object> U39 = MapUtils.getUser(39, "example39@email.com", null, "Roberto",
            "Firmino", true, "England", "Merseyside", "Forward", "1946-06-14", "39.jpg", 0);
    public static final Map<String, Object> U40 = MapUtils.getUser(40, "example40@email.com", null, "Sadio", "Mane",
            true, "England", "Merseyside", "Forward", "1946-06-14", "40.jpg", 0);
    public static final Map<String, Object> U41 = MapUtils.getUser(41, "example41@email.com", null, "Sergio", "Augero",
            true, "England", "Manchester", "Forward", "1946-06-14", "41.jpg", 0);
    public static final Map<String, Object> U42 = MapUtils.getUser(42, "example42@email.com", null, "Sergio", "Ramos",
            true, "Spain", "Madrid", "Defender", "1946-06-14", "42.jpg", 0);
    public static final Map<String, Object> U43 = MapUtils.getUser(43, "example43@email.com", null, "Heung Min", "Son",
            true, "England", "London", "Forward", "1946-06-14", "43.jpg", 0);
    public static final Map<String, Object> U44 = MapUtils.getUser(44, "example44@email.com", null, "Thibaut",
            "Courtois", true, "England", "London", "Goalkeeper", "1946-06-14", "44.jpg", 0);
    public static final Map<String, Object> U45 = MapUtils.getUser(45, "example45@email.com", null, "Toni", "Kroos",
            true, "Spain", "Madrid", "Midfielder", "1946-06-14", "45.jpg", 0);
    public static final Map<String, Object> U46 = MapUtils.getUser(46, "example46@email.com", null, "Axel", "Witsel",
            true, "Germany", "Dortmund", "Defender", "1946-06-14", "46.jpg", 0);
    public static final Map<String, Object> U47 = MapUtils.getUser(47, "example47@email.com", null, "Gerard", "Pique",
            true, "Spain", "Catalonia", "Defender", "1946-06-14", "47.jpg", 0);
    public static final Map<String, Object> U48 = MapUtils.getUser(48, "example48@email.com", null, "James",
            "Rodriguez", true, "Germany", "Munich", "Forward", "1946-06-14", "48.jpg", 0);
    public static final Map<String, Object> U49 = MapUtils.getUser(49, "example49@email.com", null, "Manuel", "Neuer",
            true, "Germany", "Munich", "Goalkeeper", "1946-06-14", "49.jpg", 0);
    public static final Map<String, Object> U50 = MapUtils.getUser(50, "example50@email.com", null, "Philippe",
            "Coutinho", true, "Spain", "Catalonia", "Midfielder", "1946-06-14", "50.jpg", 0);
    public static final Map<String, Object> U51 = MapUtils.getUser(51, "example51@email.com", null, "Alcantara",
            "Rafinha", true, "Italy", "Milan", "Defender", "1946-06-14", "51.jpg", 0);
    public static final Map<String, Object> U52 = MapUtils.getUser(52, "example52@email.com", null, "Rafinha",
            "Alcantara", true, "Germany", "Munich", "Midfielder", "1946-06-14", "52.jpg", 0);
    public static final Map<String, Object> U53 = MapUtils.getUser(53, "example53@email.com", null, "Thomas",
            "Vermaelen", true, "Spain", "Catalonia", "Defender", "1946-06-14", "53.jpg", 0);
    public static final Map<String, Object> U54 = MapUtils.getUser(54, "example54@email.com", null, "Aaron", "Long",
            true, "US", "NewYork", "Defender", "1946-06-14", "54.jpg", 1);
    public static final Map<String, Object> U55 = MapUtils.getUser(55, "example55@email.com", null, "Alejandro",
            "Romero", true, "US", "NewYork", "Midfielder", "1946-06-14", "55.jpg", 1);
    public static final Map<String, Object> U56 = MapUtils.getUser(56, "example56@email.com", null, "Alex", "Muyl",
            true, "US", "NewYork", "Midfielder", "1946-06-14", "56.jpg", 1);
    public static final Map<String, Object> U57 = MapUtils.getUser(57, "example57@email.com", null, "Anatole", "Abang",
            true, "US", "NewYork", "Forward", "1946-06-14", "57.jpg", 1);
    public static final Map<String, Object> U58 = MapUtils.getUser(58, "example58@email.com", null, "Andreas", "Ivan",
            true, "US", "NewYork", "Midfielder", "1946-06-14", "58.jpg", 1);
    public static final Map<String, Object> U59 = MapUtils.getUser(59, "example59@email.com", null, "Bradley Wright",
            "Phillips", true, "US", "NewYork", "Forward", "1946-06-14", "59.jpg", 1);
    public static final Map<String, Object> U60 = MapUtils.getUser(60, "example60@email.com", null, "Connor", "Lade",
            true, "US", "NewYork", "Forward", "1946-06-14", "60.jpg", 1);
    public static final Map<String, Object> U61 = MapUtils.getUser(61, "example61@email.com", null, "Derrick",
            "Etienne", true, "US", "NewYork", "Midfielder", "1946-06-14", "61.jpg", 1);
    public static final Map<String, Object> U62 = MapUtils.getUser(62, "example62@email.com", null, "Ethan", "Kutler",
            true, "US", "NewYork", "Defender", "1946-06-14", "62.jpg", 1);
    public static final Map<String, Object> U63 = MapUtils.getUser(63, "example63@email.com", null, "Fidel", "Escobar",
            true, "US", "NewYork", "Defender", "1946-06-14", "63.jpg", 1);
    public static final Map<String, Object> U64 = MapUtils.getUser(64, "example64@email.com", null, "Hassan", "Ndam",
            true, "US", "NewYork", "Defender", "1946-06-14", "64.jpg", 1);
    public static final Map<String, Object> U65 = MapUtils.getUser(65, "example65@email.com", null, "Kemar", "Lawrence",
            true, "US", "NewYork", "Defender", "1946-06-14", "65.jpg", 1);
    public static final Map<String, Object> U66 = MapUtils.getUser(66, "example66@email.com", null, "Luis", "Robles",
            true, "US", "NewYork", "Goalkeeper", "1946-06-14", "66.jpg", 1);
    public static final Map<String, Object> U67 = MapUtils.getUser(67, "example67@email.com", null, "Michael Amir",
            "Murillo", true, "US", "NewYork", "Defender", "1946-06-14", "67.jpg", 1);
    public static final Map<String, Object> U68 = MapUtils.getUser(68, "example68@email.com", null, "Ryan", "Meara",
            true, "US", "NewYork", "Goalkeeper", "1946-06-14", "68.jpg", 1);
    public static final Map<String, Object> U69 = MapUtils.getUser(69, "example69@email.com", null, "Sean", "Davis",
            true, "US", "NewYork", "Midfielder", "1946-06-14", "69.jpg", 1);
    public static final Map<String, Object> U70 = MapUtils.getUser(70, "example70@email.com", null, "Tim", "Parker",
            true, "US", "NewYork", "Defender", "1946-06-14", "70.jpg", 1);
    public static final Map<String, Object> U71 = MapUtils.getUser(71, "example71@email.com", null, "Tommy", "Redding",
            true, "US", "NewYork", "Defender", "1946-06-14", "71.jpg", 1);
    public static final Map<String, Object> U72 = MapUtils.getUser(72, "example72@email.com", null, "Tyler", "Adams",
            true, "US", "NewYork", "Midfielder", "1946-06-14", "72.jpg", 1);
    public static final Map<String, Object> U73 = MapUtils.getUser(73, "example73@email.com", null, "Vincent",
            "Bezecourt", true, "US", "NewYork", "Midfielder", "1946-06-14", "73.jpg", 1);
    public static final Map<String, Object> U74 = MapUtils.getUser(74, "example74@email.com", null, "Florian", "Valot",
            true, "US", "NewYork", "Midfielder", "1946-06-14", "74.jpg", 1);
    public static final Map<String, Object> U75 = MapUtils.getUser(75, "example75@email.com", null, "Daniel", "Royer",
            true, "US", "NewYork", "Midfielder", "1946-06-14", "75.jpg", 1);
    public static final Map<String, Object> U76 = MapUtils.getUser(76, "example76@email.com", null, "Evan", "Louro",
            true, "US", "NewYork", "Goalkeeper", "1946-06-14", "76.jpg", 1);
    
    public static final List<Map<String, Object>> COM1 = MapUtils.listOf(U54, U55, U56, U57, U58, U59, U60, U61, U62,
            U63, U64, U65, U66, U67, U68, U69, U70, U71, U72, U73, U74, U75, U76);

    public static final List<Map<String, Object>> DATA = MapUtils.listOf(U2, U3, U4, U5, U6, U7, U8, U9, U10, U11, U12,
            U13, U14, U15, U16, U17, U18, U19, U20, U21, U22, U23, U24, U25, U26, U27, U28, U29, U30, U31, U32, U33,
            U34, U35, U36, U37, U38, U39, U40, U41, U42, U43, U44, U45, U46, U47, U48, U49, U50, U51, U52, U53, U54,
            U55, U56, U57, U58, U59, U60, U61, U62, U63, U64, U65, U66, U67, U68, U69, U70, U71, U72, U73, U74, U75,
            U76);
}
