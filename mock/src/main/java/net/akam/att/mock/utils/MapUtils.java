package net.akam.att.mock.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.StringUtils;

import net.akam.att.mock.model.Key;

public class MapUtils {

    public static final Map<String, Object> getUser(int id, String email, String phone, String firstName,
            String lastName, boolean male,  String country, String location, String professional,String birth,
            String avatar, int comId) {
        
        Map<String, Object> map = new LinkedHashMap<>();
        
        map.put(Key.ID, id);
        
        if (!StringUtils.isEmpty(email)) {
            map.put(Key.EMAIL, email);
        }
       
        if (!StringUtils.isEmpty(phone)) {
            map.put(Key.PHONE, phone);
        }
        
        map.put(Key.FIRST_NAME, firstName);
       
        map.put(Key.LAST_NAME, lastName);
       
        map.put(Key.MALE, male);
        
        map.put(Key.BIRTH, birth);
        
        if (!StringUtils.isEmpty(country)) {
            map.put(Key.COUNTRY, country);
        }
        
        if (!StringUtils.isEmpty(location)) {
            map.put(Key.LOCATION, location);
        }
        
        if (!StringUtils.isEmpty(professional)) {
            map.put(Key.PROFRESSIONAL, professional);
        }
        
        if (!StringUtils.isEmpty(avatar)) {
            map.put(Key.AVATAR, avatar);
        }
        
        map.put(Key.COM_ID, comId);
        
        return map;
    }

    @SafeVarargs
    public static final List<Map<String, Object>> listOf(Map<String, Object>... maps) {
        List<Map<String, Object>> list = new ArrayList<>();
        list.addAll(Arrays.asList(maps));
        return list;
    }
}