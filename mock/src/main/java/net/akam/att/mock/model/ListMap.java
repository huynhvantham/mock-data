package net.akam.att.mock.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ListMap {

    private List<Map<String, Object>> data = new ArrayList<>();

    private List<String> keys = new ArrayList<>();

    public ListMap keys(String... strings) {
        keys.addAll(Arrays.asList(strings));
        return this;
    }

    public ListMap values(Object... objects) {

        Map<String, Object> map = new LinkedHashMap<>();
        for (int i = 0; i < objects.length; i++) {
            map.put(keys.get(i), objects[i]);
        }
        data.add(map);
        return this;
    }

    public List<Map<String, Object>> build() {
        return data;
    }

}
