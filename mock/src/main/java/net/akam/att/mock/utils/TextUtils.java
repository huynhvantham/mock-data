package net.akam.att.mock.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;

import org.springframework.util.StringUtils;

public class TextUtils {

    public static boolean isEmail(String s) {
        if (StringUtils.isEmpty(s)) {
            return false;
        }
        String pattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(pattern);
        java.util.regex.Matcher m = p.matcher(s);
        return m.matches();
    }

    public static boolean isPhone(String s) {
        if (StringUtils.isEmpty(s)) {
            return false;
        }
        String pattern = "^[0-9]*$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(pattern);
        java.util.regex.Matcher m = p.matcher(s);
        return m.matches() && s.length() > 9 && s.length() < 17;
    }

    public static boolean isPassword(String s) {
        if (StringUtils.isEmpty(s)) {
            return false;
        }
        return s.length() > 5 && s.length() < 33;
    }

    public static boolean in(String s, int minLength, int maxLength) {
        if (StringUtils.isEmpty(s)) {
            return minLength == 0 ? true : false;
        }
        return s.length() >= minLength && s.length() <= maxLength;
    }

    public static boolean isDateTime(String format, String value) {
        if (StringUtils.isEmpty(value)) {
            return false;
        }
        String s = value;
        if (value.indexOf("T") != -1) {
            s = s.substring(0, s.indexOf("T"));
        }
        SimpleDateFormat fmt = new SimpleDateFormat(format);
        try {
            fmt.parse(value);
            return true;
        } catch (Exception ignore) {
            return false;
        }
    }

    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    public static String urlDecode(String s) {
        try {
            return URLDecoder.decode(s, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }
}
