package net.akam.att.mock.model;

public class Search {

    public int id;
    public String email, phone, firstName, lastName, country, location, professional, birth, avatar;

    public Search(int id, String email, String phone, String firstName, String lastName, String country,
            String location, String professional, String birth, String avatar) {
        super();
        this.id = id;
        this.email = email;
        this.phone = phone;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
        this.location = location;
        this.professional = professional;
        this.birth = birth;
        this.avatar = avatar;
    }

}
