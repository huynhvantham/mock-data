package net.akam.att.mock.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.akam.att.mock.data.User;
import net.akam.att.mock.model.Body;
import net.akam.att.mock.model.Key;
import net.akam.att.mock.utils.TextUtils;

@RestController
@RequestMapping("/api")
public class UserController extends BaseController {

    @PostMapping("/authenticate")
    public ResponseEntity<?> getUser(@RequestBody Body body) {

        String username = body.string(Key.EMAIL_PHONE);
        if (!TextUtils.isEmail(username) && !TextUtils.isPhone(username)) {
            return badRequest(Key.EMAIL_PHONE, "email/phone format");
        }

        String password = body.string(Key.PASSWORD);
        if (!TextUtils.isPassword(password)) {
            return badRequest(Key.PASSWORD, "6..32 characters");
        }

        if ((username.equals("huyqv.softlink@gmail.com") || username.equals("84935491290")) && password.equals("123456")) {
            return success(User.CURRENT_USER);
        }

        return failed("username or password incorrectly");
    }
    
    @PostMapping("/users/register")
    public ResponseEntity<?> insertUser(@RequestBody Body body) {

        String firstName = body.string(Key.FIRST_NAME);
        if (!TextUtils.in(firstName, 1, 32)) {
            return badRequest(Key.FIRST_NAME, "1..32 characters");
        }

        String lastName = body.string(Key.LAST_NAME);
        if (!TextUtils.in(lastName, 1, 32)) {
            return badRequest(Key.LAST_NAME, "1..32 characters");
        }

        String birth = body.string(Key.BIRTH);
        if (!TextUtils.isDateTime("yyyy-MM-dd", birth)) {
            return badRequest(Key.BIRTH, "yyyy-MM-dd");
        }

        Boolean male = body.bool(Key.MALE);
        if (male == null) {
            return badRequest(Key.MALE);
        }

        String email = body.string(Key.EMAIL);
        String phone = body.string(Key.PHONE);
        if (!TextUtils.isEmail(email) && !TextUtils.isPhone(phone)) {
            return badRequest("phone/email", "phone/email format");
        }

        String password = body.string(Key.PASSWORD);
        if (!TextUtils.isPassword(password)) {
            return badRequest(Key.PASSWORD, "6..32 characters");
        }

        return success(User.CURRENT_USER);
    }

    @PutMapping("/users/edit")
    public ResponseEntity<?> editUser(@RequestBody Body body) {
        
        int id = body.integer(Key.ID);
        if (id == 1) {
            return badRequest(Key.FIRST_NAME, "1..32 characters");
        }
        
        String firstName = body.string(Key.FIRST_NAME);
        if (!TextUtils.in(firstName, 1, 32)) {
            return badRequest(Key.FIRST_NAME, "1..32 characters");
        }

        String lastName = body.string(Key.LAST_NAME);
        if (!TextUtils.in(lastName, 1, 32)) {
            return badRequest(Key.LAST_NAME, "1..32 characters");
        }

        String birth = body.string(Key.BIRTH);
        if (!TextUtils.isDateTime("yyyy-MM-dd", birth)) {
            return badRequest(Key.BIRTH, "yyyy-MM-dd");
        }

        Boolean male = body.bool(Key.MALE);
        if (male == null) {
            return badRequest(Key.MALE);
        }

        String email = body.string(Key.EMAIL);
        String phone = body.string(Key.PHONE);
        if (!TextUtils.isEmail(email) && !TextUtils.isPhone(phone)) {
            return badRequest("phone/email", "phone/email format");
        }

        String password = body.string(Key.PASSWORD);
        if (!TextUtils.isPassword(password)) {
            return badRequest(Key.PASSWORD, "6..32 characters");
        }

        return success(User.CURRENT_USER_UPDATED);
    }
 
    @PostMapping("/change-password")
    public ResponseEntity<?> changePassword(@RequestBody Body body) {

        String currentPassword = body.string("currentPassword");
        if (!TextUtils.isPassword(currentPassword)) {
            return badRequest("currentPassword", "6..32 characters");
        }

        String newPassword = body.string("newPassword");
        if (!TextUtils.isPassword(newPassword)) {
            return badRequest("newPassword", "6..32 characters");
        }

        if (!currentPassword.equals("123456")) {
            return failed("current password incorrectly");
        }

        if (newPassword.equals("123456")) {
            return failed("new password must be different from current password");
        }

        return success();
    }

    @GetMapping(value = "/company{id}")
    public ResponseEntity<?> searchRaw(@PathVariable(value = "id") int id) {
        return success(User.COM1);
    }

    @GetMapping(value = "/search")
    public ResponseEntity<?> searchRaw(@RequestBody Body body) {

        String keyword = body.string(Key.KEYWORD);
        Integer startIndex = body.integer(Key.START_INDEX);
        Integer endIndex = body.integer(Key.END_INDEX);
        return searchPath(keyword, startIndex, endIndex);
    }

    @GetMapping(value = "/search/param")
    public ResponseEntity<?> searchParam(@RequestParam(value = "keyword") String keyword,
            @RequestParam(value = "startIndex") Integer startIndex,
            @RequestParam(value = "endIndex") Integer endIndex) {

        return searchPath(keyword, startIndex, endIndex);
    }

    @GetMapping(value = "/search/key{keyword}/start{startIndex}/end{endIndex}")
    public ResponseEntity<?> searchPath(@PathVariable String keyword, @PathVariable Integer startIndex,
            @PathVariable Integer endIndex) {

        if (!TextUtils.in(keyword, 3, 65)) {
            return badRequest(Key.KEYWORD, "3..65 characters");
        }

        if (startIndex == null || startIndex < 1) {
            return badRequest(Key.START_INDEX, "integer");
        }

        if (endIndex == null || endIndex < 1) {
            return badRequest(Key.END_INDEX, "integer");
        }

        if (startIndex > endIndex) {
            return badRequest("startIndex must be more than endIndex");
        }

        List<Map<String, Object>> data = User.DATA;
        List<Map<String, Object>> results = new ArrayList<>();
        int maxSize = data.size();

        for (int i = startIndex; i <= endIndex; i++) {
            if (i >= maxSize) {
                break;
            }
            results.add(data.get(i));
        }

        if (results.isEmpty()) {
            return success("no result found", null);
        }

        String keywordPath = TextUtils.urlEncode(keyword);
        String message = keywordPath == null ? "success"
                : String.format("/search/key%s/start%s/end%s", keywordPath, endIndex + 1,
                        endIndex + endIndex - startIndex + 1);
        return success(message, results);

    }

    @GetMapping(value = "/{phoneOrEmail}")
    public ResponseEntity<?> find(@PathVariable String phoneOrEmail) {

        if (TextUtils.isEmail(phoneOrEmail)) {
            List<Map<String, Object>> data = User.DATA;
            for (int i = 0; i < data.size(); i++) {

                Object email = data.get(i).get(Key.EMAIL);

                if (email == null) {
                    continue;
                }

                if (email.toString().isEmpty()) {
                    continue;
                }

                if (email.toString().equals(phoneOrEmail)) {
                    return success(data.get(i));
                }
            }
            return success("no result found", null);
        }

        if (TextUtils.isPhone(phoneOrEmail)) {

            List<Map<String, Object>> data = User.DATA;

            for (int i = 0; i < data.size(); i++) {

                Object phone = data.get(i).get(Key.PHONE);

                if (phone == null) {
                    continue;
                }

                if (phone.toString().isEmpty()) {
                    continue;
                }

                if (phone.toString().equals(phoneOrEmail)) {
                    return success(data.get(i));
                }
            }
            return success("no result found", null);
        }

        return badRequest("phone/email", "phone/email format");
    }

    @PostMapping(value = "/reset-password/init")
    public ResponseEntity<?> resetPassword(@RequestBody Body body){
        
        String phoneOrEmail = body.string(Key.EMAIL_PHONE);
        if (TextUtils.isEmail(phoneOrEmail)) {
            List<Map<String, Object>> data = User.DATA;
            for (int i = 0; i < data.size(); i++) {

                Object email = data.get(i).get(Key.EMAIL);

                if (email == null) {
                    continue;
                }

                if (email.toString().isEmpty()) {
                    continue;
                }

                if (email.toString().equals(phoneOrEmail)) {
                    return success(data.get(i));
                }
            }
            return failed("email not exist");   
        }

        if (TextUtils.isPhone(phoneOrEmail)) {

            List<Map<String, Object>> data = User.DATA;

            for (int i = 0; i < data.size(); i++) {

                Object phone = data.get(i).get(Key.PHONE);

                if (phone == null) {
                    continue;
                }

                if (phone.toString().isEmpty()) {
                    continue;
                }

                if (phone.toString().equals(phoneOrEmail)) {
                    return success(data.get(i));
                }
            }
            return failed("phone not exist"); 
        }
        
        return badRequest("phone/email", "phone/email format");
    }
}
