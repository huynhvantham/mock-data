package net.akam.att.mock.data;

import net.akam.att.mock.model.Json;
import net.akam.att.mock.model.Key;

public class Company {

    public static final Json CURRENT_COM = new Json().put(Key.ID, 1).put(Key.NAME, "New York Red Bull SC")
            .put(Key.DESCRIPTION, "American professional soccer club based in Harrison, New Jersey")
            .put(Key.PROFRESSIONAL, "sport").put(Key.CONTACT_EMAIL, "redbull@outlook.com")
            .put(Key.CONTACT_PHONE, "8419001080").put(Key.COUNTRY, "US").put(Key.LOCATION, "NewYork")
            .put(Key.ADDRESS, "220 W 18th St, New York, NY 10011, USA");

}
