package net.akam.att.mock.data;

import java.util.List;
import java.util.Map;

import net.akam.att.mock.model.Key;
import net.akam.att.mock.model.ListMap;

public class Place {

    public static final List<Map<String, Object>> COM1 = new ListMap().keys(Key.ID, Key.NAME, Key.ADDRESS, Key.COM_ID)
            .values(1, "Red bull Arena Stadium", "600 Cape May St, Harrison, NJ 07029", 1)
            .values(2, "Training Facility", "24 Melanie Lane, Whippany, NJ", 1)
            .values(3, "Red Bull Gaming House", "2 Chance St, London E1 6JT", 1).build();
}