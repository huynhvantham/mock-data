package net.akam.att.mock.model;

public class Response<T> {

    public int status;

    public String message;

    public T body;

    public Response(int status, String message, T body) {
        this.status = status;
        this.message = message;
        this.body = body;
    }

    public Response() {
        this(0, "Some thing went wrong :(", null);
    }

}
