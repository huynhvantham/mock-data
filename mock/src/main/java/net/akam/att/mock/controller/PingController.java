package net.akam.att.mock.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class PingController {

    @GetMapping("/ping")
    public String index() {
        return "\n\t" + "\n\t      __       __   ___       __       ___      ___  "
                + "\n\t     /  \\     |/ | /   )     /  \\     |   \\    /   |        "
                + "\n\t    /    \\    |: |/   /     /    \\     \\   \\  //   |       "
                + "\n\t   /' /\\  \\   |    __/     /' /\\  \\    /\\\\  \\/.    |    "
                + "\n\t  //  __'  \\  |// _  \\    //  __'  \\  |: \\.        |       "
                + "\n\t /   /  \\\\  \\ |: | \\  \\  /   /  \\\\  \\ |.  \\    /:  |  "
                + "\n\t(___/    \\___)|__|  \\__)(___/    \\___)|___|\\__/|___|       "
                + "\n\tVersion:1.0.0-DEV \t\t\t\t\t Copy right 2019";

    }

}
