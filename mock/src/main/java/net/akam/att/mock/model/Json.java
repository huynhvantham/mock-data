package net.akam.att.mock.model;

import java.util.LinkedHashMap;
import java.util.Map;

public class Json {

    Map<String, Object> map = new LinkedHashMap<>();

    public Json put(String key, Object value) {
        map.put(key, value);
        return this;
    }

    public Map<String, Object> build() {
        return map;
    }
}
