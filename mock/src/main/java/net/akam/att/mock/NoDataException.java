package net.akam.att.mock;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(HttpStatus.NO_CONTENT)
public class NoDataException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public NoDataException(String message) {
        super(message);
    }
}
