package net.akam.att.mock.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.akam.att.mock.data.Company;
import net.akam.att.mock.model.Body;
import net.akam.att.mock.model.Key;
import net.akam.att.mock.utils.TextUtils;

@RestController
@RequestMapping("/api/companies")
public class CompanyController extends BaseController {

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody Body body) {

        String name = body.string(Key.NAME);
        if (!TextUtils.in(name, 1, 50)) {
            return badRequest(Key.NAME, "1..50 characters");
        }

        String description = body.string(Key.DESCRIPTION);
        if (!TextUtils.in(description, 1, 240)) {
            return badRequest(Key.DESCRIPTION, "1..240 characters");
        }

        String email = body.string(Key.CONTACT_EMAIL);
        if (!TextUtils.isEmail(email)) {
            return badRequest(Key.CONTACT_EMAIL);
        }

        String phone = body.string(Key.CONTACT_PHONE);
        if (!TextUtils.isPhone(phone)) {
            return badRequest(Key.CONTACT_PHONE);
        }

        String country = body.string(Key.COUNTRY);
        if (!TextUtils.in(country, 2, 2)) {
            return badRequest(Key.COUNTRY, "country ISO 3166-1 alpha 2 code");
        }

        String location = body.string(Key.LOCATION);
        if (!TextUtils.in(location, 3, 50)) {
            return badRequest(Key.LOCATION, "3..50 characters");
        }

        String address = body.string(Key.ADDRESS);
        if (!TextUtils.in(address, 1, 120)) {
            return badRequest(Key.ADDRESS, "1..140 characters");
        }

        return success(Company.CURRENT_COM);
    }
}
